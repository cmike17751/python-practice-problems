class Animal:
    def __init__(self, num_legs, prime_color):
        self.num_legs = num_legs
        self.prime_color = prime_color
    def describe(self):
        return(self.__class__.__name__
        + " has "
        +str(self.num_legs)
        +" legs and is primarily "
        +self.prime_color)

animal = Animal(6, "green")

class Dog(Animal):
    def speak(self):
        return "Bark!"
doggo = Dog(4,"brindle")
print(doggo.describe())
print(doggo.speak())

class Cat(Animal):
    def speak(self):
        return "Meow"


class Snake(Animal):
    def speak(self):
        return "I'm a slithery snek"
